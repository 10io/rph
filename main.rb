# frozen_string_literal: true

require 'json'
require 'open3'
require 'timeout'

def execute(cmd)
  stdout, stderr, status = Open3.capture3(cmd)

  raise "Kaboom: #{stderr}" unless status.success?

  stdout
end

def is_ok?(result)
  status = json_parse(result, 'status')

  return false if status == nil

  status == 'ok'
end

def id_of(result)
  json_parse(result, 'id')
end

def is_finished?(result)
  status = json_parse(result, 'status')

  return false if status == nil

  status == 'finished'
end

def empty_results_from?(result)
  results = results_of(result)

  results.empty?
end

def results_of(result)
  json_parse(result, 'result')
end

def json_parse(raw_json, field)
  return nil if raw_json == nil

  parsed = JSON.parse(raw_json)
  parsed[field]
end

cmd = "curl -X POST -H 'Content-Type: application/octet-stream' --data-binary @#{ENV['PH_ARCHIVE_FILE']} --user #{ENV['PH_USERNAME']}:#{ENV['PH_PASSWORD']} #{ENV['PH_BASE_URL']}/monitor/dependency/yarn"
puts cmd
result = execute(cmd)

raise "Not ok command" unless is_ok?(result)

cmd = "curl -v --user #{ENV['PH_USERNAME']}:#{ENV['PH_PASSWORD']} \"#{ENV['PH_BASE_URL']}/?id=#{id_of(result)}\""
result = nil

Timeout.timeout(30, message = "This is taking too long!") do
  while(!is_finished?(result)) do
    result = execute(cmd)
    sleep(2)
  end
end

raise "Results not empty: #{results_of(result)}" unless empty_results_from?(result)
